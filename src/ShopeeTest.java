import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ShopeeTest {
	private static ChromeDriver driver;
 	WebElement element;
 	List<Product> productDetails = new ArrayList<>();
 	 @BeforeClass
     public static void openBrowser(){
 		
         System.setProperty("webdriver.chrome.driver", "D:/chromedriver.exe");
         driver = new ChromeDriver();
         driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		} 
 	 
 	@Test
    public void search(){

		 System.out.println("Starting test " + new Object(){}.getClass().getEnclosingMethod().getName());
	     driver.get("https://shopee.com.my/");	
	     driver.findElement(By.xpath("//*[@class=\"shopee-button-outline shopee-button-outline--primary-reverse \"]")).click(); 
	     driver.findElement(By.xpath("//*[@class=\"shopee-popup__close-btn\"]")).click(); 
	 	 driver.findElement(By.className("shopee-searchbar-input__input")).sendKeys("Iphone 11"); 
	 	 driver.findElement(By.xpath("//*[@class=\"btn btn-solid-primary btn--s btn--inline\"]")).click();
	 	 System.out.println(driver.getTitle());
	 
	 	 
	 	List<WebElement> shopeeListResult= driver.findElements(By.xpath(".//div[@class='_1ObP5d']"));
	 	List<WebElement> productNameList = driver.findElements(By.xpath(".//div[@class='yQmmFK _1POlWt _36CEnF']"));
 		List<WebElement> productPriceList =driver.findElements(By.xpath(".//div[@class='_32hnQt']"));
 		List<WebElement> productLinkList = driver.findElements(By.xpath(".//*/div[@class=\"col-xs-2-4 shopee-search-item-result__item\"]/a"));

 		System.out.println(shopeeListResult.size());
 		System.out.println(productNameList.size());
 		System.out.println(productPriceList.size());
 		System.out.println(productLinkList.size());
 		
 		for(int i = 0; i < productNameList.size(); i++) {
 			Product prod = new Product();
 	        prod.productName = productNameList.get(i).getText();
 	        prod.productPrice = productPriceList.get(i).getText();
 	        prod.productLink =  productLinkList.get(i).getAttribute("href");
 	        
 	        System.out.println(prod.productName);
	 		System.out.println(prod.productPrice);
	 		System.out.println(prod.productLink);
	        productDetails.add(prod);
 		}
	 	//if you want to print matching results
	 	System.out.println("Ending Shopee " + new Object(){}.getClass().getEnclosingMethod().getName());
    }
 	
	
 
}
