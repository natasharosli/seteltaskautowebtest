import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class EbayTest {
	private static ChromeDriver driver;
 	WebElement element;

 	 @BeforeClass
     public static void openBrowser(){
 		
         System.setProperty("webdriver.chrome.driver", "D:/chromedriver.exe");
         driver = new ChromeDriver();
         driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		} 
 	 
 	@Test
    public void search(){
 		
 		 List<Product> productDetails = new ArrayList<>();
		 System.out.println("Starting test " + new Object(){}.getClass().getEnclosingMethod().getName());
	     driver.get("https://ebay.com");	
	    
	 	 driver.findElement(By.id("gh-ac")).sendKeys("Iphone 11"); 
	 	 driver.findElement(By.id("gh-btn")).click();
	 	 System.out.println(driver.getTitle());
	 	 assertEquals("iphone 11 | eBay", driver.getTitle());
	 	
	 	List<WebElement> productNameList= driver.findElements(By.className("s-item__title"));
	 	List<WebElement> productPriceList= driver.findElements(By.className("s-item__price"));
	 	List<WebElement> productLinkList= driver.findElements(By.className("s-item__link"));
	 	
	 	for(int i = 0; i < productNameList.size(); i++) {
	 		Product prod = new Product();
 	        prod.productName = productNameList.get(i).getText();
 	        prod.productPrice = productPriceList.get(i).getText();
 	        prod.productLink = productLinkList.get(i).getAttribute("href");
 	        		
 	        System.out.println(prod.productName);
 	 		System.out.println(prod.productPrice);
 	 		System.out.println(prod.productLink);
 	        productDetails.add(prod);
 		}
	 	
	 	 System.out.println("Ending Ebay " + new Object(){}.getClass().getEnclosingMethod().getName());
   
 	
 	}
 	

}
