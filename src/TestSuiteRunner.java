import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

public class TestSuiteRunner {

	public static void main (String[] args) {
		Result result= JUnitCore.runClasses(WebTestSuite.class);
		
		System.out.println(result.wasSuccessful());
	}

}
